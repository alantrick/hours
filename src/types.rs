//!
//! Types for (mostly) internal use
//!

use chrono::{Datelike, NaiveDateTime};

pub type Step = i32;
pub type Year = i32;
pub type WeekNumber = u8;
pub type DayNumber = u8;

#[derive(Clone, Debug, PartialEq)]
pub enum Month {
    Jan,
    Feb,
    Mar,
    Apr,
    May,
    Jun,
    Jul,
    Aug,
    Sep,
    Oct,
    Nov,
    Dec,
}

impl std::str::FromStr for Month {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Jan" => Ok(Month::Jan),
            "Feb" => Ok(Month::Feb),
            "Mar" => Ok(Month::Mar),
            "Apr" => Ok(Month::Apr),
            "May" => Ok(Month::May),
            "Jun" => Ok(Month::Jun),
            "Jul" => Ok(Month::Jul),
            "Aug" => Ok(Month::Aug),
            "Sep" => Ok(Month::Sep),
            "Oct" => Ok(Month::Oct),
            "Nov" => Ok(Month::Nov),
            "Dec" => Ok(Month::Dec),
            _ => Err("Unknown month".to_owned()),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Weekday {
    Mo,
    Tu,
    We,
    Th,
    Fr,
    Sa,
    Su,
}

impl std::str::FromStr for Weekday {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Mo" => Ok(Weekday::Mo),
            "Tu" => Ok(Weekday::Tu),
            "We" => Ok(Weekday::We),
            "Th" => Ok(Weekday::Th),
            "Fr" => Ok(Weekday::Fr),
            "Sa" => Ok(Weekday::Sa),
            "Su" => Ok(Weekday::Su),
            _ => Err("Unknown week day".to_owned()),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct MonthRange {
    pub year: Option<Year>,
    pub end: Year,
    // whether the range is open ended (has a plus at the end)
    pub open: bool,
    pub step: Step,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Modifier {
    pub state: State,
    pub comment: String,
}

impl Modifier {
    pub fn is_open(&self) -> bool {
        self.state == State::Open
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum State {
    Open,
    Closed,
    Unknown,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Selector {
    Always,
    Sometimes(RangeSelector),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RangeSelector {
    pub year: Vec<YearRange>,
    // pub monthday: State,
    // pub week: State,
    // pub weekday: State,
    // pub time: Vec<TimeRange>,
}

impl RangeSelector {
    pub fn matches(&self, dt: &NaiveDateTime) -> bool {
        self.year.iter().any(|y| y.matches(dt))
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TimeRange {
    pub year: State,
    pub monthday: State,
    pub week: State,
    pub weekday: State,
    pub time: String,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct YearRange {
    start: Year,
    end: Option<Year>,
    step: Step,
}

impl YearRange {
    // just a single year
    pub fn single(y: Year) -> Self {
        Self {
            start: y,
            end: Some(y),
            step: 1,
        }
    }
    // a single year, open-ended
    pub fn open(start: Year) -> Self {
        Self {
            start,
            end: None,
            step: 1,
        }
    }
    // start, end, optional step
    pub fn range(start: Year, end: Year, step: Option<Step>) -> Self {
        Self {
            start,
            end: Some(end),
            step: step.unwrap_or(1),
        }
    }

    pub fn matches(&self, dt: &NaiveDateTime) -> bool {
        let year = dt.year();
        if let Some(e) = self.end {
            if e < year {
                return false;
            }
        }
        if self.start > year {
            return false;
        }
        ((year - self.start) % self.step) == 0
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum RuleSequenceType {
    Normal,     // normal rule, follows ';'
    Additional, // additional rule, follows ','
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RuleSequence {
    pub sequence_type: RuleSequenceType,
    pub selector: Selector,
    pub modifier: Modifier,
}

impl RuleSequence {
    pub fn matches(&self, dt: &NaiveDateTime) -> bool {
        match self.selector {
            Selector::Always => true,
            Selector::Sometimes(ref rs) => rs.matches(dt),
        }
    }

    pub fn normal(selector: Selector, modifier: Modifier) -> Self {
        Self {
            sequence_type: RuleSequenceType::Normal,
            selector,
            modifier,
        }
    }

    pub fn additional(selector: Selector, modifier: Modifier) -> Self {
        Self {
            sequence_type: RuleSequenceType::Additional,
            selector,
            modifier,
        }
    }

    pub fn ends_matching(&self) -> bool {
        !(self.sequence_type == RuleSequenceType::Normal && self.modifier.is_open())
    }
}
