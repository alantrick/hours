//!
//! Opening hours implementation for Rust
//!
//! Specification: https://wiki.openstreetmap.org/wiki/Key:opening_hours/specification
//!

// #[macro_use]
// extern crate nom;

use nom::{
    branch::alt,
    bytes::complete::{is_not, tag, take, take_while_m_n},
    character::complete::{char, digit1},
    combinator::{all_consuming, map, opt},
    error::ErrorKind,
    multi::{many0, separated_list},
    sequence::{delimited, pair, preceded, separated_pair, terminated},
    Err, IResult,
};

use crate::types;
const QUOTE_CHAR: char = '"';
const QUOTE_STR: &str = "\"";

// Helper functions

///
/// take a certain number of chars and run it through parse
///
fn parse_n<T: std::str::FromStr>(n: usize, i: &str) -> IResult<&str, T> {
    let (new_i, t) = take(n)(i)?;

    match t.parse() {
        Ok(res) => Ok((new_i, res)),
        Err(_) => Err(Err::Error((i, ErrorKind::ParseTo))),
    }
}

///
/// take a certain number of digit chars and run it through parse
///
fn parse_n_digits<T: std::str::FromStr>(n: usize, i: &str) -> IResult<&str, T> {
    let (i, digits) = take_while_m_n(n, n, |x: char| x.is_digit(10))(i)?;

    match digits.parse() {
        Ok(res) => Ok((i, res)),
        Err(_) => Err(Err::Error((i, ErrorKind::Digit))),
    }
}

fn parse_digit1<T: std::str::FromStr>(i: &str) -> IResult<&str, T> {
    let (i, digits) = digit1(i)?;

    match digits.parse() {
        Ok(res) => Ok((i, res)),
        Err(_) => Err(Err::Error((i, ErrorKind::Digit))),
    }
}

// Basic elements

/// Comment parser
///
/// Comments are quote delimited and have not escape mechanism
fn parse_comment(i: &str) -> IResult<&str, &str> {
    delimited(
        char(QUOTE_CHAR),
        alt((is_not(QUOTE_STR), tag(""))),
        char(QUOTE_CHAR),
    )(i)
}

#[test]
fn parse_comment_test_empty() {
    assert_eq!(parse_comment("\"\""), Ok(("", "")));
}

#[test]
fn parse_comment_test_good() {
    assert_eq!(parse_comment("\"this is good\""), Ok(("", "this is good")));
}

#[test]
fn parse_comment_test_non_comment() {
    assert_eq!(
        parse_comment("foo"),
        Err(Err::Error(("foo", ErrorKind::Char)))
    );
}

#[test]
fn parse_comment_test_with_quote() {
    assert_eq!(
        parse_comment("\"this \" isn't good\""),
        Ok((" isn't good\"", "this "))
    );
}

///
/// Parse a year
///
/// Years are supposed to be 4 digit numbers greater than 1900. We're leanient and
/// allow years before then.
///
fn parse_year(i: &str) -> IResult<&str, types::Year> {
    parse_n_digits(4usize, i)
}

#[test]
fn parse_year_test() {
    assert_eq!(parse_year("2000"), Ok(("", 2000)));
    assert_eq!(parse_year("0000"), Ok(("", 0)));
    assert_eq!(parse_year("9999"), Ok(("", 9999)));
    assert_eq!(parse_year("99999"), Ok(("9", 9999)));
    // this year isn't valid because it's too short
    assert_eq!(
        parse_year("190"),
        Err(Err::Error(("190", ErrorKind::TakeWhileMN)))
    );
}

///
/// Parse a month
///
fn parse_month(i: &str) -> IResult<&str, types::Month> {
    parse_n(3usize, i)
}

#[test]
fn parse_month_test() {
    assert_eq!(parse_month("Jan"), Ok(("", types::Month::Jan)));
    assert_eq!(parse_month("Dec"), Ok(("", types::Month::Dec)));
    assert_eq!(parse_month("Jun_"), Ok(("_", types::Month::Jun)));
    // this year isn't valid because it's too short
    assert_eq!(
        parse_month("123"),
        Err(Err::Error(("123", ErrorKind::ParseTo)))
    );
}

///
/// Parse a week number
///
fn parse_weeknum(i: &str) -> IResult<&str, types::WeekNumber> {
    parse_n_digits(2usize, i)
}

///
/// Parse a day of month number
///
///
fn parse_daynum(i: &str) -> IResult<&str, types::DayNumber> {
    parse_n_digits(2usize, i)
}

///
/// Parse a month
///
fn parse_weekday(i: &str) -> IResult<&str, types::Weekday> {
    parse_n(2usize, i)
}

#[test]
fn parse_weekday_test() {
    assert_eq!(parse_weekday("Mo"), Ok(("", types::Weekday::Mo)));
    assert_eq!(parse_weekday("Su"), Ok(("", types::Weekday::Su)));
    assert_eq!(parse_weekday("Tu"), Ok(("", types::Weekday::Tu)));
    assert_eq!(parse_weekday("Th"), Ok(("", types::Weekday::Th)));
    assert_eq!(parse_weekday("WeWe"), Ok(("We", types::Weekday::We)));
    assert_eq!(
        parse_weekday("123"),
        Err(Err::Error(("123", ErrorKind::ParseTo)))
    );
}

// Year selector

fn parse_step(i: &str) -> IResult<&str, types::Step> {
    preceded(tag("/"), parse_digit1)(i)
}

fn parse_year_range_single(i: &str) -> IResult<&str, types::YearRange> {
    map(parse_year, types::YearRange::single)(i)
}
fn parse_year_range_open(i: &str) -> IResult<&str, types::YearRange> {
    map(terminated(parse_year, tag("+")), |y| {
        types::YearRange::open(y)
    })(i)
}
fn parse_year_range_range(i: &str) -> IResult<&str, types::YearRange> {
    map(
        pair(
            separated_pair(parse_year, tag("-"), parse_year),
            opt(parse_step),
        ),
        |((a, b), s)| types::YearRange::range(a, b, s),
    )(i)
}
fn parse_year_range(i: &str) -> IResult<&str, types::YearRange> {
    alt((
        parse_year_range_range,
        parse_year_range_open,
        parse_year_range_single,
    ))(i)
}

#[test]
fn parse_year_range_test() {
    assert_eq!(
        parse_year_range("2000"),
        Ok(("", types::YearRange::single(2000)))
    );
    assert_eq!(
        parse_year_range("2000+"),
        Ok(("", types::YearRange::open(2000)))
    );
    assert_eq!(
        parse_year_range("2000-2010/2"),
        Ok(("", types::YearRange::range(2000, 2010, Some(2))))
    );
    assert_eq!(
        parse_year_range("abc"),
        Err(Err::Error(("abc", ErrorKind::TakeWhileMN)))
    );
}

// Month Selector
// TODO

// Week Selector
// TODO

// Weekday Selector
// TODO

// Time Selector
// TODO

// Selector

fn parse_selector_sometimes(i: &str) -> IResult<&str, types::Selector> {
    let (i, year) = separated_list(tag(";"), parse_year_range)(i)?;
    Ok((i, types::Selector::Sometimes(types::RangeSelector { year })))
}
fn parse_selector_always(i: &str) -> IResult<&str, types::Selector> {
    map(tag("24/7"), |_| types::Selector::Always)(i)
}
fn parse_selector(i: &str) -> IResult<&str, types::Selector> {
    alt((parse_selector_always, parse_selector_sometimes))(i)
}

#[test]
fn parse_slector_test_always() {
    assert_eq!(parse_selector("24/7"), Ok(("", types::Selector::Always,)));
}

// Rule Modifier section

fn parse_state(i: &str) -> IResult<&str, types::State> {
    map(
        alt((tag("open"), tag("closed"), tag("off"), tag("unknown"))),
        |s: &str| match s {
            "open" => types::State::Open,
            "closed" => types::State::Closed,
            "off" => types::State::Closed,
            "unknown" => types::State::Unknown,
            _ => types::State::Unknown, // this shouldn't happen
        },
    )(i)
}

#[test]
fn parse_state_test() {
    assert_eq!(parse_state("open"), Ok(("", types::State::Open)));
    assert_eq!(parse_state("closed"), Ok(("", types::State::Closed)));
    assert_eq!(parse_state("off"), Ok(("", types::State::Closed)));
    assert_eq!(parse_state("unknown"), Ok(("", types::State::Unknown)));
    assert_eq!(parse_state(""), Err(Err::Error(("", ErrorKind::Tag))));
}

fn parse_modifier(i: &str) -> IResult<&str, types::Modifier> {
    let (i, state_opt) = opt(parse_state)(i)?;
    let (i, _) = opt(tag(" "))(i)?;
    let (i, c_opt) = opt(parse_comment)(i)?;
    Ok((
        i,
        types::Modifier {
            state: state_opt.unwrap_or(types::State::Open),
            comment: c_opt.unwrap_or("").to_owned(),
        },
    ))
}

#[test]
fn parse_modifier_test_open_no_comment() {
    assert_eq!(
        parse_modifier("open"),
        Ok((
            "",
            types::Modifier {
                state: types::State::Open,
                comment: "".to_owned(),
            }
        ))
    );
}

#[test]
fn parse_modifier_test_open_comment() {
    assert_eq!(
        parse_modifier("open \"hi\""),
        Ok((
            "",
            types::Modifier {
                state: types::State::Open,
                comment: "hi".to_owned(),
            }
        ))
    );
}

#[test]
fn parse_modifier_test_only_comment() {
    assert_eq!(
        parse_modifier("\"comment\""),
        Ok((
            "",
            types::Modifier {
                state: types::State::Open,
                comment: "comment".to_owned(),
            }
        ))
    );
}

// rule sequence

fn parse_rule_sequence(i: &str) -> IResult<&str, (types::Selector, types::Modifier)> {
    separated_pair(parse_selector, opt(tag(" ")), parse_modifier)(i)
}

fn parse_first_normal_rule_sequence(i: &str) -> IResult<&str, types::RuleSequence> {
    map(parse_rule_sequence, |(a, b)| {
        types::RuleSequence::normal(a, b)
    })(i)
}
fn parse_other_normal_rule_sequence(i: &str) -> IResult<&str, types::RuleSequence> {
    map(preceded(tag("; "), parse_rule_sequence), |(a, b)| {
        types::RuleSequence::normal(a, b)
    })(i)
}
fn parse_additional_rule_sequence(i: &str) -> IResult<&str, types::RuleSequence> {
    map(preceded(tag(", "), parse_rule_sequence), |(a, b)| {
        types::RuleSequence::additional(a, b)
    })(i)
}
// NOTE: I can't figure out how || is supposed to work, so I'm not implementing it right now

// domain

fn partial_parse_domain(i: &str) -> IResult<&str, Vec<types::RuleSequence>> {
    let (i, first_rule) = parse_first_normal_rule_sequence(i)?;
    let (i, mut extra_rules) = many0(alt((
        parse_other_normal_rule_sequence,
        parse_additional_rule_sequence,
    )))(i)?;
    extra_rules.reverse();
    extra_rules.push(first_rule);
    Ok((i, extra_rules))
}

pub fn parse_domain(i: &str) -> IResult<&str, Vec<types::RuleSequence>> {
    all_consuming(partial_parse_domain)(i)
}

#[test]
fn parse_domain_test_always() {
    assert_eq!(
        parse_domain("24/7"),
        Ok((
            "",
            vec![types::RuleSequence::normal(
                types::Selector::Always,
                types::Modifier {
                    state: types::State::Open,
                    comment: "".to_owned()
                }
            )]
        ))
    );
}

#[test]
fn parse_domain_test_year_open() {
    assert_eq!(
        parse_domain("2010 open"),
        Ok((
            "",
            vec![types::RuleSequence::normal(
                types::Selector::Sometimes(types::RangeSelector {
                    year: vec![types::YearRange::single(2010)]
                }),
                types::Modifier {
                    state: types::State::Open,
                    comment: "".to_owned()
                }
            )]
        ))
    );
}
#[test]
fn parse_domain_test_year_two() {
    assert_eq!(
        parse_domain("2010 open; 2012 open"),
        Ok((
            "",
            vec![
                types::RuleSequence::normal(
                    types::Selector::Sometimes(types::RangeSelector {
                        year: vec![types::YearRange::single(2012),]
                    }),
                    types::Modifier {
                        state: types::State::Open,
                        comment: "".to_owned()
                    }
                ),
                types::RuleSequence::normal(
                    types::Selector::Sometimes(types::RangeSelector {
                        year: vec![types::YearRange::single(2010),]
                    }),
                    types::Modifier {
                        state: types::State::Open,
                        comment: "".to_owned()
                    }
                ),
            ]
        ))
    );
}
