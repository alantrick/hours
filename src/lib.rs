//!
//! Opening hours implementation for Rust
//!
//! Specification: https://wiki.openstreetmap.org/wiki/Key:opening_hours/specification
//!

use chrono;
use chrono::DateTime;
use chrono_tz::Tz;

mod parsers;
mod types;

// #[derive(Clone, Debug, PartialEq, Eq)]
pub struct Hours {
    // this will be the reverse of the parsed sequence
    pub rules: Vec<types::RuleSequence>,
    pub tz: Tz,
}

impl Hours {
    pub fn from(s: &str, tz: Tz) -> Result<Self, String> {
        match parsers::parse_domain(s) {
            Ok((_i, r)) => Ok(Hours { rules: r, tz }),
            Err(_e) => Err("Error parsing hours. Error handling not implemented".to_owned()),
        }
    }

    pub fn at(self: &Self, dt: &DateTime<Tz>) -> types::Modifier {
        let mut matched_rule: Option<&types::RuleSequence> = None;

        for rule in self.rules.iter() {
            if rule.matches(&dt.naive_local()) {
                matched_rule = Some(&rule);
                if rule.ends_matching() {
                    break;
                }
            }
        }
        match matched_rule {
            None => types::Modifier {
                state: types::State::Closed,
                comment: "".to_owned(),
            },
            Some(r) => r.modifier.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono_tz::Africa::Johannesburg;
    use chrono::TimeZone;

    fn expect_open(m: types::Modifier) {
        if !m.is_open() {
            panic!("Time should be open, but isn't")
        }
    }

    fn expect_closed(m: types::Modifier) {
        if m.is_open() {
            panic!("Time should be closed, but isn't")
        }
    }

    #[test]
    fn test_always_open() {
        let is_open = Hours::from("24/7", Johannesburg)
            .unwrap()
            .at(&Johannesburg.ymd(2000, 1, 1).and_hms(12, 0, 0))
            .is_open();
        assert_eq!(is_open, true);
    }

    #[test]
    fn test_range_exclusion() {
        let hours = Hours::from("2001-2003 open; 2002 closed", Johannesburg).unwrap();
        expect_open(hours.at(&Johannesburg.ymd(2001, 1, 1).and_hms(12, 0, 0)));
        let result = hours.at(&Johannesburg.ymd(2002, 1, 1).and_hms(12, 0, 0));
        expect_closed(hours.at(&Johannesburg.ymd(2002, 1, 1).and_hms(12, 0, 0)));
        expect_open(hours.at(&Johannesburg.ymd(2003, 1, 1).and_hms(12, 0, 0)));
    }

    #[test]
    fn test_year_step() {
        let hours = Hours::from("2001-2011/3 open", Johannesburg).unwrap();
        expect_open(hours.at(&Johannesburg.ymd(2001, 1, 1).and_hms(12, 0, 0)));
        expect_closed(hours.at(&Johannesburg.ymd(2002, 1, 1).and_hms(12, 0, 0)));
        expect_open(hours.at(&Johannesburg.ymd(2004, 1, 1).and_hms(12, 0, 0)));
    }

}
